#include <stdbool.h>

struct regex;

struct regex* re_compile(char* str);
bool re_match(const struct regex* regex, const char* text);
void re_destroy(struct regex* regex);
