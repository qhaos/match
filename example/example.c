#include <ctype.h>
#include <match.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "linenoise.h"

char* getinput(char* prompt) {
  char* ret = linenoise(prompt);

  if(!ret || *ret == '\0') {
    return NULL;
  }

  return ret;
}

int main() {
  char* re = getinput("enter regex: ");

  if(!re) {
    fputs("encountered unexpected EOF or out of memory\n", stderr);
    return 1;
  }

  char* input = getinput("enter input: ");

  if(!input) {
    linenoiseFree(re);
    fputs("encountered unexpected EOF or out of memory\n", stderr);
    return 1;
  }

  struct regex* r = re_compile(re);

  if(!r) {
    perror("re_compile");
    re_destroy(r);
    return 1;
  }

  static const char* const boolstr[2] = {"no match", "match"};
  puts(boolstr[re_match(r, input)]);

  linenoiseFree(re);
  linenoiseFree(input);
  re_destroy(r);
}
