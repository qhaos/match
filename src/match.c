#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "match.h"


struct regex {
  struct regex* next;

  enum {
    RE_FRONT,
    RE_END,
    RE_CHAR,
    RE_CLASS,
    RE_ANY,
  } type;

  enum {
    RE_SINGLE,
    RE_STAR,
    RE_PLUS,
    RE_OPT,
  } mod;

  union {
    int ch;
    char* class;
  };

} regex;


static char parse_escape(char* str, char** endptr) {
  /* TODO: add multibyte support and other special characters */
  /* assume that str[-1] == '\\' */
  if(endptr)
    *endptr = str + 1;
  switch(str[0]) {
    /* hex escapes */
    case 'x':
      return strtoul(str + 1, endptr, 16);
    case 'n':
      return '\n';
    case 't':
      return '\t';
    default:
      return str[0];
  }
}

bool re_matchhere(const struct regex* regex, const char* text);
bool re_matchstar(const struct regex* regex, const char* text);

struct regex* re_compile(char* str) {
  if(!str[0])
    return NULL;

  struct regex* ret = (struct regex*)calloc(sizeof(regex), 1);

  if(!ret)
    return ret;

  struct regex* tail = ret;
  struct regex* save = tail;


  if(*str == '^') {
    tail->type = RE_FRONT;
    tail->next = (struct regex*)calloc(sizeof(regex), 1);
    tail = tail->next;
    str++;
  }

  while(str[0]) {
    if(!tail) {
      re_destroy(ret);
      return NULL;
    }

    tail->ch = str[0];

    switch(tail->ch) {
      case '$':
        tail->type = RE_END;
      break;
      case '.':
        tail->type = RE_ANY;
      break;
      case '[': {
        /* TODO: expand beyond 256 chars */
        str++;
        tail->type = RE_CLASS;
        char tmp[256];
        size_t i = 0;
        while(i < 256) {
          if(*str == ']')
            break;

          if(*str == '\\')
            tmp[i++] = parse_escape(++str, &str);
          else
            tmp[i++] = *str++;
        }

        if(i) {
          tail->class = malloc(i+1);
          strncpy(tail->class, tmp, i);
        }

      }
      break;
      case '\\':
        tail->ch = parse_escape(++str, &str);
        tail->type = RE_CHAR;
      break;
      default:
        tail->type = RE_CHAR; 
      break;
    }

    str++;
    switch(str[0]) {
      case '+':
        tail->mod = RE_PLUS;
        str++;
      break;
      case '?':
        tail->mod = RE_OPT;
        str++;
      break;
      case '*':
        tail->mod = RE_STAR;
        str++;
      break;
      case '\0':
        return ret;
      break;
      default:
        tail->mod = RE_SINGLE;
      break;
    }
    
    save = tail;
    tail->next = (struct regex*)calloc(sizeof(regex), 1);
    tail = tail->next;
  }

  free(tail);
  save->next = NULL;

  return ret;
}

bool re_match(const struct regex* regex, const char* text) {
  if(regex->type == RE_FRONT) {
    return re_matchhere(regex->next, text);
  }

  do {
    if(re_matchhere(regex, text))
      return true;
  } while(*text++);

  return false;
}

bool re_matchsingle(int ch, const struct regex* const regex) {
  switch(regex->type) {
    case RE_CHAR:
      return ch == regex->ch;
    case RE_CLASS:
      return strchr(regex->class, ch);
    case RE_ANY:
      return true;
    case RE_STAR:
      return ch == '\0';
    case RE_FRONT:
      return false;
  }

  return false;
}

bool re_matchhere(const struct regex* regex, const char* text) {
  if(!regex)
    return true;

  switch(regex->mod) {
    case RE_OPT:
      if(re_matchsingle(*text, regex)) {
        if(re_matchhere(regex->next, text+1)) {
          return true;
        }
      }

      return re_matchhere(regex->next, text);
    break;
    case RE_SINGLE:
      if(re_matchsingle(*text, regex))
        return re_matchhere(regex->next, text+1);
    break;
    case RE_PLUS:
      if(!re_matchsingle(*text++, regex))
        return false;
    // fallthrough
    case RE_STAR:
      return re_matchstar(regex, text);
    break;
  }

  return false;
}

bool re_matchstar(const struct regex* regex, const char* text) {
  do {
    if(re_matchhere(regex->next, text)) 
	return true;
  } while(*text && re_matchsingle(*text++, regex));


  return false;
}

void re_destroy(struct regex* regex) {
  struct regex* save = regex;
  while(save) {
    if(regex->type == RE_CLASS)
      free(regex->class);
    regex = regex->next;
    free(save);
    save = regex;
  }
}
